/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataView {

	static WebDriver driver;
	boolean acceptNextAlert = true;
	WebDriverWait wait;
	static Logger logger;
	public static Properties elementProperties;

  public DataView() {
	  this.driver = QuickTestMain.driver;
	  this.logger = QuickTestMain.logger;
	  this.elementProperties = QuickTestMain.elementProperties;
  }
  
  @Test(dataProvider = "EmployeeSummary")
  private void doNavigateToEmployeeSummary(String dvForTimecard, String employeesummaryloadconfirmation) {
	    wait = new WebDriverWait(driver, 30);
	  // Load Data View for Timecard Navigation 
	    driver.get(dvForTimecard);
	    //wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy((By) driver.findElements(By.xpath("//div[contains(@class, 'inner-cell')]")))); 
	    try {
	       wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(employeesummaryloadconfirmation))));
               Utils.logMsg("Employee Summary Data View display Successful");
	    }
	    catch (Exception ex) {
	    	Utils.logError("Unable to locate element in DV Employee Summary page load confirmation. Test will continue...");
                Utils.logError(ex.getMessage());
	    }
  }
  
  @Test
  public void screenshotEmployeeSummaryDV() {
	  thinkTime(1000);
	  try {
		Utils.takeSnapShot(driver, "Screenshots/employeesummaryDataView.png") ;
	} catch (Exception e) {
		// TODO Auto-generated catch block
		Utils.logError("Unable to take screenshot of Employee Summary Data View. Test will continue...");
		e.printStackTrace();
	}  
  }
  
  private static void thinkTime(long t) {
	  try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
  
  @DataProvider(name = "EmployeeSummary")
  public static Object[] loadEmployeeSummaryProperties() {
      return new Object[][] {{QuickTestMain.url+"timecard-landing-page#/dataView", elementProperties.getProperty("employeesummaryloadconfirmation")}};
  }
}