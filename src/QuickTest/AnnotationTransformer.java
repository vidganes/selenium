/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

public class AnnotationTransformer implements IAnnotationTransformer {
	static Logger logger;
        //private static ArrayList<String> selectedTestsList;

	@Override
	public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor, Method testMethod) {
		// TODO Auto-generated method stub
		logger = QuickTestMain.logger;
                ArrayList<String> selectedTestsList = QuickTestMain.selectedTestsList;
		logger.info("TestNG methods Annotation : " + testMethod.getName());               
		
		    if (testMethod.getName() == "doLogon" ) {
		    	annotation.setPriority(0);
			annotation.setEnabled(true);	
		    }
		    
		    if (testMethod.getName() == "screenshotHomePage") {
		    	annotation.setPriority(1);
		   	annotation.setEnabled(true);
		    }
				    
		    if (testMethod.getName() == "doNavigateToEmployeeSummary" ) {
		    	annotation.setPriority(2);
		    	annotation.setEnabled(true);
		    }
		    			    
		    if (testMethod.getName() == "screenshotEmployeeSummaryDV" ) {
                        annotation.setPriority(3);
		    	annotation.setEnabled(true);
		    }
		    
		    if (testMethod.getName() == "doOpenTimecard") {
		    	annotation.setPriority(4);
			annotation.setEnabled(true);
		    }
		    
		    if (testMethod.getName() == "screenshotTimecard") {
		    	annotation.setPriority(5);
			annotation.setEnabled(true);
		    }
		    
		    if (testMethod.getName() == "currentPayPeriod") {
		    	annotation.setPriority(6);
		   	annotation.setEnabled(true);
		    }
                    
		    if (testMethod.getName() == "previousPayPeriod") {
                        if (selectedTestsList.contains("Sign Off")) {
		    	    annotation.setPriority(17);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
		    if (testMethod.getName() == "nextPayPeriod") {
		    	annotation.setPriority(207);
		   	annotation.setEnabled(false);
		    }
		    		    
		    if (testMethod.getName() == "timecardPunch") {
                        if (selectedTestsList.contains("In Punch") ||
                                selectedTestsList.contains("Out Punch")) 
                        {
                            annotation.setPriority(7);
                            annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
		    
		    if (testMethod.getName() == "punchWithTransfer") {
                        if (selectedTestsList.contains("In Punch") && 
                                selectedTestsList.contains("Punch Transfer"))
                        {
                            annotation.setPriority(8);
                            annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
		    
                    if (testMethod.getName() == "punchWithComment") {
                        if (((selectedTestsList.contains("In Punch") || 
                                selectedTestsList.contains("Out Punch"))) && selectedTestsList.contains("Punch Comment"))
                        {
		    	    annotation.setPriority(9);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
                    
		    if (testMethod.getName() == "paycodeEdit") {
                        if (selectedTestsList.contains("Pay Code Edit")) {
		    	    annotation.setPriority(10);
			    annotation.setEnabled(true);
		        }
                        else
                            annotation.setEnabled(false);
                    }
                    
                    if (testMethod.getName() == "reviewExceptions") {
                         if (selectedTestsList.contains("Review Exceptions")) {
		    	    annotation.setPriority(11);
			    annotation.setEnabled(true);
		        }
                        else
                            annotation.setEnabled(false);		    	 
		    }
		    		    
		    
		    
		    if (testMethod.getName() == "timecardTotals") {
                        if (selectedTestsList.contains("Totals Tab")) {
		    	    annotation.setPriority(12);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
		    
		    if (testMethod.getName() == "timecardAudit") {
		    	if (selectedTestsList.contains("Audit Tab")) {
		    	    annotation.setPriority(13);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }		  
		    
		    if (testMethod.getName() == "timecardAccruals") {
		    	if (selectedTestsList.contains("Accruals Tab")) {
		    	    annotation.setPriority(14);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }		    		   
		    
		    if (testMethod.getName() == "timecardHCorrections") {
		    	if (selectedTestsList.contains("Historical Corrections Tab")) {
		    	    annotation.setPriority(15);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }		   
		    
		    if (testMethod.getName() == "timecardWorkSummary") {
		    	if (selectedTestsList.contains("Work Summary Tab")) {
		    	    annotation.setPriority(16);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    } 		    		    		    
		    
		    if (testMethod.getName() == "signOff") {
                        if (selectedTestsList.contains("Sign Off")) {
		    	    annotation.setPriority(18);
			    annotation.setEnabled(true);
                        }
                        else
                            annotation.setEnabled(false);
		    }
		    
		    if (testMethod.getName() == "removeSignOff") {
		    	annotation.setPriority(19);
			   	annotation.setEnabled(false);
		    }
		    
		    if (testMethod.getName() == "doLogoff" ) {
		    	annotation.setPriority(900);
		    	annotation.setEnabled(true);
		    }
	}
}