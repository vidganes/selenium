/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;


public class Logoff {

	static WebDriver driver;
	boolean acceptNextAlert = true;
	static StringBuffer verificationErrors = new StringBuffer();
	static String URL;
	WebDriverWait wait;
	static Logger logger;
	public static Properties elementProperties;
	public static final String ELEMENT_PROPERTIES_FILE = "Properties/element.properties";

    public Logoff() {
	  this.driver = QuickTestMain.driver;
	  this.logger = QuickTestMain.logger;
	  this.elementProperties = QuickTestMain.elementProperties;
    }
  
	@Test(dataProvider = "Logoff")
	public void doLogoff(String menu, String signout) {
	  wait = new WebDriverWait(driver, 30);
	  try {
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(menu ))));   
	    driver.findElement(By.className(menu)).click();
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id(signout ))));
	    driver.findElement(By.id(signout)).click();
	  }
	  catch (Exception ex) {
	    	Utils.logError("Unable to LogOff. Browser will quit... " );
	    }
	  driver.quit();
	}
	 
	@DataProvider(name = "Logoff")
	public static Object[] logoffProperties() {
	   return new Object[][] {{elementProperties.getProperty("menu"), elementProperties.getProperty("signout")}};
	}
	
}