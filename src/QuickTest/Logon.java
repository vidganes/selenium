/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import java.time.Duration;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;



public class Logon {
	
	static WebDriver driver;
	boolean acceptNextAlert = true;
	static StringBuffer verificationErrors = new StringBuffer();
	static String URL;
        static String username;
        static String password;
	WebDriverWait wait;
	static Logger logger;
	public static Properties elementProperties;
	public static final String ELEMENT_PROPERTIES_FILE = "Properties/element.properties";

  public Logon() {
	  this.driver = QuickTestMain.driver;
	  this.logger = QuickTestMain.logger;
	  this.elementProperties = QuickTestMain.elementProperties;
          this.username = QuickTestMain.username;
          this.password = QuickTestMain.password;
          this.URL = QuickTestMain.url;
  }
  
	/*
  public void testLogon() {
	   
	      doLogon();
	      doNavigateToEmployeeSummary();
	      doOpenTimecard();
	      doLogoff();
	    
	    /**
	    //Home Page
	    driver.findElement(By.xpath("//i[@class='item-icon icon-k-home']")).click();
	    //driver.startTransaction("Mgr HomePage");
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='manage-schedule-content']" ))));                                                                 
	    //driver.stopTransaction();
	    
	    thinkTime(2000);
	    wait = new WebDriverWait(driver, 30);
	    //Full Schedule
	    //driver.startTransaction("Full Schedule Load");
	    driver.findElement(By.xpath("/html[1]/body[1]/div[1]/main[1]/div[1]/div[1]/krn-home[1]/div[1]/div[2]/ul[1]/li[5]/krn-tile[1]/div[1]/card[1]/section[1]/manageschedulecard[1]/div[1]/div[3]/a[1]/i[1]")).click();    
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//span[@class='schedule-timestamp-lastrefreshed__label ng-binding']" ))));  
	    //driver.findElement(By.xpath("//span[@class='schedule-timestamp-lastrefreshed__label ng-binding']"));
	    //driver.stopTransaction();    
	    
	    
	    thinkTime(3000); 
	  //Control Center
	    wait = new WebDriverWait(driver, 30);
	    //driver.startTransaction("Control Center");
	    driver.findElement(By.xpath("//span[@id='alertsCenter_icon']")).click();   
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("/html[1]/body[1]/div[1]/main[1]/div[1]/div[1]/div[2]/div[2]/div[1]/div[4]/div[2]/div[1]/div[5]" ))));                                                              
	    //driver.stopTransaction();
	    
	    thinkTime(1000);
	    //Home Page
	    driver.findElement(By.xpath("//i[@class='item-icon icon-k-home']")).click();
	    //driver.startTransaction("Mgr HomePage");
	    wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='manage-schedule-content']" ))));                                                                 
	    //driver.stopTransaction();
	    
	    
	    
	    thinkTime(1000);
	    //driver.startTransaction("Mgr Logout");
	    driver.findElement(By.xpath("//header/div/button/span[2]")).click();
	    driver.findElement(By.id("profileSignOut_text")).click();
	    //driver.stopTransaction();
	     
	    
	    driver.close();
  }
 **/
  
  @Test(dataProvider = "Logon1")
  public void doLogon(String menu) {
	    wait = new WebDriverWait(driver, 30);
            
            try {
                driver.get(URL);
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("loginButton_0"))));	
                driver.findElement(By.id("idToken1")).clear();
                driver.findElement(By.id("idToken1")).sendKeys(username);
                driver.findElement(By.id("idToken2")).click();
                driver.findElement(By.id("idToken2")).clear();
                driver.findElement(By.id("idToken2")).sendKeys(password);
                QuickTestMain.thinkTime(200);
                driver.findElement(By.id("loginButton_0")).click();
                wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(menu)))); 
                Utils.logMsg("Logon Successful");
            }
            catch (NoSuchElementException ex) {
		    	Utils.logError("Unable to Login to application. Aborting test... " );
                        Utils.logError(ex.getMessage());
		    	QuickTestMain.abortTestSuite = 1;
            }           
  }
  
  @Test
  public void screenshotHomePage() { 
	wait = new WebDriverWait(driver, 30);
        try {
	        wait.until(ExpectedConditions.visibilityOfAllElements(
                        driver.findElements(By.xpath("//*[contains(@class, 'btn widget-button-icon')]")))); 
		Utils.takeSnapShot(driver, "Screenshots/HomePage.png") ;
	     }
        catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		Utils.logError("Failed to take screenshot of HomePage after logon. Test will continue...");
                Utils.logError(e.getMessage());
	}  
  }
  
  private static void thinkTime(long t) {
	  try {
			Thread.sleep(t);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
  
  public boolean waitForJSandJQueryAngularToLoad() {

	    @SuppressWarnings("deprecation")
		WebDriverWait wait = (WebDriverWait) new FluentWait<WebDriver>(driver)
		          .withTimeout(Duration.ofSeconds(30))
	    		  .pollingEvery(Duration.ofMillis(250));
	    // wait for jQuery to load
	    ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
	    	
	      @Override
	      public Boolean apply(WebDriver driver) {
	        try {
	        	return (Boolean) ((JavascriptExecutor)driver).executeScript("return (window.jQuery != null) && (jQuery.active == 0);");
	        }
	        catch (Exception e) {
	          // no jQuery present
	          return true;
	        }
	      }
	    };

	    // wait for Javascript to load
	    ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
	      @Override
	      public Boolean apply(WebDriver driver) {
	        return ((JavascriptExecutor)driver).executeScript("return document.readyState")
	        .toString().equals("complete");
	      }
	    };

	    ExpectedCondition<Boolean> angularLoad = new ExpectedCondition<Boolean>() {
		      @Override
		      public Boolean apply(WebDriver driver) {
	    return Boolean.valueOf(((JavascriptExecutor)driver).executeScript("return (window.angular != undefined) && (angular.element(document).injector().get('https').pendingRequests == 0)").toString());
		      }
	    };
	  return wait.until(jQueryLoad) && wait.until(jsLoad) && wait.until(angularLoad);
	}
    
      @DataProvider(name = "Logon")
      public static Object[] loadLogonProperties() {
          return new Object[][] {{elementProperties.getProperty("username"), elementProperties.getProperty("password"), 
              elementProperties.getProperty("menu")}};
      }
      
      @DataProvider(name = "Logon1")
      public static Object[] loadLogonMenu() {
          return new Object[][] {{elementProperties.getProperty("menu")}};
      }
}
