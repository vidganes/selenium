/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import static org.testng.Assert.fail;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.TestNG;
import org.testng.annotations.AfterSuite;

/**
 *
 * @author GSwaminathan
 */
public class QuickTestMain extends javax.swing.JFrame {

    /**
     * Creates new form QuickTestMain
     */
    
    static WebDriver driver;
    boolean acceptNextAlert = true;
    static StringBuffer verificationErrors = new StringBuffer();
    WebDriverWait wait;
    static Logger logger = Logger.getLogger(Logon.class.getName());
    public static Properties elementProperties = new Properties();
    public static final String ELEMENT_PROPERTIES_FILE = "Properties/element.properties";
    static String arrTestMethods;
    static String username;
    static String password;
    static String url;
    static int abortTestSuite = 0;
    private ArrayList<String> availableTestsList;
    static ArrayList<String> selectedTestsList;
    
    @SuppressWarnings("deprecation")    
    public QuickTestMain() {
        setupLogging();
        loadProperties();
        initComponents();
        initDisplay();
    }
    
    private void runTestSuite() {
        populateSelectedTests();
        if (selectedTestsList.size() > 0)
        {
            setupBrowser();
            TestNG testSuite = new TestNG();
            testSuite.addListener(new Listener());
            username = usernameText.getText();
            password = passwordText.getText();
            url = URLText.getText();
            //testSuite.addListener(new TestSuiteMethodInterceptor());
            //testSuite.addListener(new TestSuiteAnnotationTransformer());
            testSuite.setAnnotationTransformer(new AnnotationTransformer());
            testSuite.setDefaultSuiteName("Test Suite");
            testSuite.setDefaultTestName("Test");
            testSuite.setTestClasses(new Class[] { Logon.class, DataView.class, Timekeeping.class, Logoff.class });
            //testSuite.setOutputDirectory("/Users/pankaj/temp/testng-output");
            startTestButton.setEnabled(false);
            testSuite.run();    
            startTestButton.setEnabled(false);
            JOptionPane.showMessageDialog(null, "Test Completed ");
            startTestButton.setEnabled(true);
        }
    }
    
    
    private static void setupLogging() {

        try {
            File f = new File("Properties/log4j.properties");
            FileInputStream is = new FileInputStream(f);
            PropertyConfigurator.configure(is);

        } catch (IOException ex) {
            logger.info("Failed to load the log4j properties file. " + ex);
        }         
        logger.info("********************Selenium TESTNG Logging Initialized********************");
    }
	
    private void setupBrowser() {
        String CHROME_DRIVER_PATH = "ChromeDriver/chromedriver.exe";
        final File file = new File(CHROME_DRIVER_PATH);
        System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());  
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        if (browserModeHeadless.isSelected())
          chromeOptions.addArguments("--headless");  
        //chromeOptions.addArguments("--headless");
        driver = new ChromeDriver(chromeOptions);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
    
    private static void loadProperties() {

        try {
            elementProperties.load(new FileInputStream(ELEMENT_PROPERTIES_FILE));
        } catch (FileNotFoundException e) {
           // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
           // TODO Auto-generated catch block
            e.printStackTrace();
            logger.info("********************Unable to load the element.properties file********************");  
        }
    }
            
    private void initDisplay() {
        moduleComboBox.setSelectedItem("Timekeeping");
        populateTimekeepingTests();
        DefaultTableModel availableTestsTableModel = (DefaultTableModel) availableTestsTable.getModel();
        availableTestsTableModel.setRowCount(0);
        initAvailableTests();
        initSelectedTests();
        DefaultTableModel selectedTestsTableModel = (DefaultTableModel) selectedTestsTable.getModel();
        selectedTestsTableModel.setRowCount(0);
        startTestButton.setEnabled(false);
    }
    
    @AfterSuite
    public void afterClass() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
          fail(verificationErrorString);
        }
    }
	
    public static void thinkTime(long t) {
        try {
                Thread.sleep(t);
        } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }
    }

    private void populateTimekeepingTests() {
        availableTestsList = new ArrayList();
        availableTestsList.add("In Punch");
        availableTestsList.add("Out Punch");
        availableTestsList.add("Punch Transfer");
        availableTestsList.add("Punch Comment");
        availableTestsList.add("Pay Code Edit");
        availableTestsList.add("Totals Tab");
        availableTestsList.add("Audit Tab");
        availableTestsList.add("Historical Corrections Tab");
        availableTestsList.add("Work Summary Tab");
        availableTestsList.add("Accruals Tab");
        availableTestsList.add("Review Exceptions");
        availableTestsList.add("Sign Off");
        availableTestsList.add("Remove Sign Off");
    }
    
    private void populateSelectedTests() {
        DefaultTableModel selectedTestsTableModel = (DefaultTableModel) selectedTestsTable.getModel();   
        selectedTestsList = new ArrayList();        
        
        if (selectedTestsTable.getRowCount() > 0) {
            for (int i = 0; i < selectedTestsTable.getRowCount(); i++) {
                selectedTestsList.add(selectedTestsTable.getValueAt(i, 0).toString());
            }
        }
    }
    
    private void initAvailableTests() {
        DefaultTableModel selectedTestsTableModel = (DefaultTableModel) selectedTestsTable.getModel();    
        DefaultTableModel availableTestsTablemodel = (DefaultTableModel) availableTestsTable.getModel();
        
        if (selectedTestsTable.getSelectedRowCount() > 0) {
            int selectedRows[] = selectedTestsTable.getSelectedRows();             
            for (int i : selectedRows) {
                String item = selectedTestsTable.getValueAt(i, 0).toString();
                //System.out.println(i + " " + item);
                availableTestsTablemodel.addRow(new Object[] {item});                
            }
            
            int numRows = selectedTestsTableModel.getRowCount();
            for (int j = numRows; j >= 0; j--) {
              final int key = j;
              if (Arrays.stream(selectedRows).anyMatch(i -> i == key)) {
                  selectedTestsTableModel.removeRow(j);
                  selectedTestsTable.revalidate();
              }
            }
        }
        else {
            for (String item : availableTestsList) {
                availableTestsTablemodel.addRow(new Object[] {item});  
            }
        }
        if (selectedTestsTableModel.getRowCount() > 0)
                startTestButton.setEnabled(true);
        else
                startTestButton.setEnabled(false);
            
    }
    
    private void initSelectedTests() {
        DefaultTableModel selectedTestsTableModel = (DefaultTableModel) selectedTestsTable.getModel();    
        DefaultTableModel availableTestsTablemodel = (DefaultTableModel) availableTestsTable.getModel();
        
        if (availableTestsTable.getSelectedRowCount() > 0) {
            int selectedRows[] = availableTestsTable.getSelectedRows();
            System.out.println("Selected Rows:" + selectedRows.length);
            for (int i : selectedRows) {
                String item = availableTestsTable.getValueAt(i, 0).toString();
                //System.out.println(i + " " + item);
                selectedTestsTableModel.addRow(new Object[] {item});                
            }
            
            int numRows = availableTestsTablemodel.getRowCount();
            for (int j = numRows; j >= 0; j--) {
              final int key = j;
              if (Arrays.stream(selectedRows).anyMatch(i -> i == key)) {
                  availableTestsTablemodel.removeRow(j);
                  availableTestsTable.revalidate();
              }
            }
            if (selectedTestsTableModel.getRowCount() > 0)
                startTestButton.setEnabled(true);
            else
                startTestButton.setEnabled(false);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        moduleComboBox = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        browserModeInteractive = new javax.swing.JRadioButton();
        browserModeHeadless = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        URLText = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        usernameText = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        passwordText = new javax.swing.JPasswordField();
        jScrollPane1 = new javax.swing.JScrollPane();
        availableTestsTable = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        selectedTestsTable = new javax.swing.JTable();
        moveRightButton = new javax.swing.JButton();
        moveLeftButton = new javax.swing.JButton();
        startTestButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Quick Test");

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("  Select Module");
        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        moduleComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Timekeeping", "Scheduling", "Attendance", "Leave", "Reporting" }));
        moduleComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moduleComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Select Browser Mode");

        buttonGroup1.add(browserModeInteractive);
        browserModeInteractive.setSelected(true);
        browserModeInteractive.setText("Interactive");

        buttonGroup1.add(browserModeHeadless);
        browserModeHeadless.setText("Headless");

        jLabel3.setText(" Logon URL");

        URLText.setText("https://dmm-test01.cfn.mykronos.com/");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText(" Username");

        usernameText.setText("SeanIvan");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText(" Password");

        passwordText.setText("Kr0n0s@Cloud");

        availableTestsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Available Tests"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(availableTestsTable);

        selectedTestsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Selected Tests"
            }
        ));
        jScrollPane2.setViewportView(selectedTestsTable);

        moveRightButton.setText(">");
        moveRightButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveRightButtonActionPerformed(evt);
            }
        });

        moveLeftButton.setText("<");
        moveLeftButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                moveLeftButtonActionPerformed(evt);
            }
        });

        startTestButton.setText("Start Test");
        startTestButton.setEnabled(false);
        startTestButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startTestButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(URLText)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(moduleComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(browserModeInteractive)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(browserModeHeadless))
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(397, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(passwordText))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(usernameText, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(moveRightButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(moveLeftButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(192, 192, 192))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(255, 255, 255)
                .addComponent(startTestButton)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(URLText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(moduleComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(browserModeInteractive)
                    .addComponent(browserModeHeadless))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(usernameText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(passwordText, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(157, 157, 157)
                        .addComponent(moveRightButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(moveLeftButton))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addGap(18, 18, 18)
                .addComponent(startTestButton)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void moduleComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moduleComboBoxActionPerformed
        // TODO add your handling code here:
        DefaultTableModel availableTestsTableModel = (DefaultTableModel) availableTestsTable.getModel();
        availableTestsTableModel.setRowCount(0);
        DefaultTableModel selectedTestsTableModel = (DefaultTableModel) selectedTestsTable.getModel();
        selectedTestsTableModel.setRowCount(0);
        startTestButton.setEnabled(false);
        if (moduleComboBox.getSelectedItem() == "Timekeeping") {
            populateTimekeepingTests();
        }
        else
        {
            availableTestsList = new ArrayList();
        }
        initAvailableTests();
    }//GEN-LAST:event_moduleComboBoxActionPerformed

    private void moveLeftButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveLeftButtonActionPerformed
        // TODO add your handling code here:
        initAvailableTests();
    }//GEN-LAST:event_moveLeftButtonActionPerformed

    private void moveRightButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_moveRightButtonActionPerformed
        // TODO add your handling code here:
        initSelectedTests();
    }//GEN-LAST:event_moveRightButtonActionPerformed

    private void startTestButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startTestButtonActionPerformed
        // TODO add your handling code here:
        runTestSuite();
    }//GEN-LAST:event_startTestButtonActionPerformed
   
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(QuickTestMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(QuickTestMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(QuickTestMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(QuickTestMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new QuickTestMain().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField URLText;
    private javax.swing.JTable availableTestsTable;
    private javax.swing.JRadioButton browserModeHeadless;
    private javax.swing.JRadioButton browserModeInteractive;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox<String> moduleComboBox;
    private javax.swing.JButton moveLeftButton;
    private javax.swing.JButton moveRightButton;
    private javax.swing.JPasswordField passwordText;
    private javax.swing.JTable selectedTestsTable;
    private javax.swing.JButton startTestButton;
    private javax.swing.JTextField usernameText;
    // End of variables declaration//GEN-END:variables
}