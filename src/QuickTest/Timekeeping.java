/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package QuickTest;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Timekeeping {
	
    static WebDriver driver;
    boolean acceptNextAlert = true;
    static StringBuffer verificationErrors = new StringBuffer();
    static String URL;
    WebDriverWait wait;
    static Logger logger;
    public static Properties elementProperties;
    public static final String ELEMENT_PROPERTIES_FILE = "Properties/element.properties";
    WebElement activeElement;
    int timecardEditFailure = 0;

    public Timekeeping() {
              this.driver = QuickTestMain.driver;
              this.logger = QuickTestMain.logger;
              this.elementProperties = QuickTestMain.elementProperties;
    }
	
    @Test(dataProvider = "OpenTimecard")
    public void doOpenTimecard(String confirmTimecardload, String selectEmpforTimecard) {
        wait = new WebDriverWait(driver, 30);
       
        try {		    	
            activeElement = driver.findElement(By.xpath(selectEmpforTimecard));	   
            activeElement.click();
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(confirmTimecardload))));
            Utils.logMsg("Open Timecacrd Successful");
            }	    	 		     
        catch (Exception ex) {
            Utils.logError("Unable to open timecard from Employee Summary data view. Aborting test...");
            QuickTestMain.abortTestSuite = 1;
        }
    }	
	
    @Test
    public void screenshotTimecard() {
        if (QuickTestMain.abortTestSuite == 0) {
          wait = new WebDriverWait(driver, 30);   
          Utils.thinkTime(1000);          
          try {
            Utils.takeSnapShot(driver, "Screenshots/timecardBeforeEdit.png");
          } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Utils.logError("Failed to take screenshot of Timecard before edit. Test will continue...");
          } 
        }
        else
          Utils.logError("Skipping screenshotTimecard() due to Abort signal...");		  
    }
		  
    @Test(dataProvider = "EditTimecard")
    public void timecardPunch(String inpunchCell, String outpunchCell, String savetimecard, String inpunchdtm, String outpunchdtm,
                              String rowDelete) {
        if (QuickTestMain.abortTestSuite == 0) {
            Utils.thinkTime(1000);
            wait = new WebDriverWait(driver, 30);

            try {
                
                /*
                // Delete row before entering punches
                activeElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(rowDelete))));
                activeElement.click();
                */
                    // Enter In Punch
                if (QuickTestMain.selectedTestsList.contains("In Punch")) {
                    activeElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(inpunchCell))));
                    activeElement.click();

                    Utils.thinkTime(1000);
                    //activeElement = driver.findElement(By.id(inpunchCell));
                    driver.switchTo().activeElement().sendKeys(inpunchdtm);
                }
                    // Enter Out Punch
                if (QuickTestMain.selectedTestsList.contains("Out Punch")) {                
                    activeElement = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(outpunchCell))));
                    activeElement.click();                   
                    Utils.thinkTime(1000);                                   
                    driver.switchTo().activeElement().sendKeys(outpunchdtm);
                    Utils.thinkTime(1000);
                }
            } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        Utils.logError("Unable enter In and Out punch on the timecard.");
                        timecardEditFailure = 1;
            } 

            // Wait few seconds for the Save button to get active
            Utils.thinkTime(1000);
            if (timecardEditFailure == 0) {
               saveTimecardEdit("Punch");
               Utils.logMsg("Timecard Punch Successful");
            }
        }
        else
            Utils.logError("Skipping timecardPunch() due to Abort signal...");		
    }
	  
    @Test(dataProvider = "PunchTransferText")
    public void punchWithTransfer(String punchTransferCell, String punchTransferText) {
        // make sure in punch exists before trying to enter transfer
        if (QuickTestMain.abortTestSuite == 0 && timecardEditFailure == 0 &&
                QuickTestMain.selectedTestsList.contains("In Punch")) {           
            wait = new WebDriverWait(driver, 30);
            
            // Wait a second and Enter Transfer
            Utils.thinkTime(1000);
            try 
            {
                driver.findElement(By.id(punchTransferCell)).click();
                activeElement = driver.findElement(By.id(punchTransferCell));
                driver.switchTo().activeElement().sendKeys(punchTransferText);
                Actions action = new Actions(driver);
                activeElement = driver.findElement(By.id("3_outpunch"));
                activeElement.click();
                //activeElement.sendKeys(Keys.ENTER);
                // Wait for the Save button to get active
                Utils.thinkTime(1000);   
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Utils.logError("Unable enter punch transfer.");
                timecardEditFailure = 1;
            }
        }
        else
            Utils.logError("Skipping punchWithTransfer() due to Abort signal or Timecard Punch Edit failure..."); 
        
        if (timecardEditFailure == 0) {
               saveTimecardEdit("Punch Transfer");
               Utils.logMsg("Punch Transfer Successful");
        }
  }

    @Test(dataProvider = "CommentsText")
    public void punchWithComment(String timecardCommentsIcon, String timecardCommentsTextbox, String comment,
                                       String commentApply, String outpunchCell, String inpunchCell) {
            Utils.thinkTime(1000);
            wait = new WebDriverWait(driver, 30);
            
            if (QuickTestMain.abortTestSuite == 0 && timecardEditFailure == 0)
            {      
                try 
                {
                    if (QuickTestMain.selectedTestsList.contains("In Punch"))
                    {
                        // Right click on in Punch to bring up the menu
                        activeElement = driver.findElement(By.id(inpunchCell));
                        Actions action = new Actions(driver);
                        action.contextClick(activeElement).perform();
                    }
                    else
                    {
                        if (QuickTestMain.selectedTestsList.contains("Out Punch"))
                        {
                            // Right click on out Punch to bring up the menu
                            activeElement = driver.findElement(By.id(outpunchCell));
                            Actions action = new Actions(driver);
                            action.contextClick(activeElement).perform();
                        } 
                    }

                    Utils.thinkTime(500);
                    // Click on Comment menu icon
                    driver.findElement(By.xpath(timecardCommentsIcon)).click();

                    Utils.thinkTime(500);
                    // Locate comment field in the slide out and enter comment
                    activeElement = driver.findElement(By.xpath(timecardCommentsTextbox));
                    Utils.thinkTime(500);

                    // search and select comment
                    if (activeElement.isDisplayed() || activeElement.isEnabled()) {
                        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath(timecardCommentsTextbox))));
                        wait.until(ExpectedConditions.elementToBeClickable(activeElement));
                        driver.findElement(By.xpath(timecardCommentsTextbox)).click();
                            Utils.thinkTime(500);			  
                            driver.findElement(By.xpath("//input[@id='comments_1_kcomment_kcombo_editCommentsList_searchbox']")).sendKeys(comment);		    
                            Utils.thinkTime(500);
                            driver.findElement(By.xpath("//span[text()=" + "'" + comment + "'" + "]")).click();
                    }

                    Utils.thinkTime(500);
                    // Click on Apply Comment
                    driver.findElement(By.xpath(commentApply)).click();
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable enter punch comment.");
                    driver.findElement(By.xpath("//button[@id='comment_cancel']")).click();
                    timecardEditFailure = 1;
                }
                // Wait for the Save button to get active
                Utils.thinkTime(1000);   
            }
            else
                Utils.logError("Skipping punchWithComment() due to Abort signal or Timecard Punch Edit failure..."); 
            
            if (timecardEditFailure == 0) {
                saveTimecardEdit("Punch Comment");         
                Utils.logMsg("Punch Comment Successful");
            }
    }

    
    @Test(dataProvider = "PaycodeEditText")
    public void paycodeEdit(String paycodeCell, String amountCell, String paycode, String amount) {
        wait = new WebDriverWait(driver, 30);

        // Wait a second and Enter Transfer
        Utils.thinkTime(1000);
        if (QuickTestMain.abortTestSuite == 0) 
        { 
            driver.findElement(By.id(paycodeCell)).click();
            activeElement = driver.findElement(By.id(paycodeCell));
            driver.switchTo().activeElement().sendKeys(paycode);

            driver.findElement(By.id(amountCell)).click();
            activeElement = driver.findElement(By.id(amountCell));
            driver.switchTo().activeElement().sendKeys(amount);
            // Wait few seconds for the Save button to get active
            Utils.thinkTime(1000);  
            saveTimecardEdit("Pay Code Edit");
            Utils.logMsg("Pay Code Edit Successful");
        }
        else
            Utils.logError("Skipping paycodeEdit() due to Abort signal or Timecard Punch Edit failure...");          
    }
    
          
    @Test(dataProvider = "ReviewExceptions")
    public void reviewExceptions(String markAsReviewedIcon, String exception1, String exception2, 
                                 String exception3, String exception4 ) {
        Utils.thinkTime(1000);
        wait = new WebDriverWait(driver, 15);
        if (QuickTestMain.abortTestSuite == 0 && timecardEditFailure == 0)
        {      
            try {
            // search for exceptions
               // Uxexcused absence exception
                if (driver.findElements(By.xpath(exception1)).size() > 0) {
                    activeElement = driver.findElement(By.xpath(exception1));
                    Actions action = new Actions(driver);
                    action.contextClick(activeElement).perform();
                    Utils.thinkTime(1000);
                    Utils.logMsg("Unexcused absence exception review Successful");
                }
                // Missed Out punch exception
                if (driver.findElements(By.xpath(exception2)).size() > 0) {
                    activeElement = driver.findElement(By.xpath(exception2));
                    Actions action = new Actions(driver);
                    action.contextClick(activeElement).perform();
                    Utils.thinkTime(1000);
                    Utils.logMsg("Missed Out punch exception review Successful");
                }
                // Missed In Punch exception
                if (driver.findElements(By.xpath(exception3)).size() > 0) {
                    activeElement = driver.findElement(By.xpath(exception3));
                    Actions action = new Actions(driver);
                    action.contextClick(activeElement).perform();
                    Utils.logMsg("Missed In punch exception review Successful");
                    Utils.thinkTime(1000);
                }
                // Unscheduled exception
                if (driver.findElements(By.xpath(exception4)).size() > 0) {
                    activeElement = driver.findElement(By.xpath(exception4));
                    Actions action = new Actions(driver);
                    action.contextClick(activeElement).perform();
                    Utils.thinkTime(1000);
                    Utils.logMsg("Unscheduled exception review Successful");
                    // Click on mark as reviewed menu icon
                    driver.findElement(By.xpath(markAsReviewedIcon)).click();
                    Utils.thinkTime(500);
                    saveTimecardEdit("Mark As Reviewed");
                }                                 
                
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Utils.logError("Unable mark exception as reviewed or there is no exception found");
                timecardEditFailure = 1;
            }
        }
        else
            Utils.logError("Skipping reviewExceptions() due to Abort signal or Timecard Punch Edit failure..."); 
            
  }

    @Test(dataProvider = "SignOff")
    public void signOff(String signoffIcon) {		   
        wait = new WebDriverWait(driver, 30);
        if (QuickTestMain.abortTestSuite == 0)
        {      
            try {
                // Click on sign off icon
                Utils.thinkTime(2000);
                driver.findElement(By.xpath(signoffIcon)).click();
                Utils.logMsg("Sign Off Successful");
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Utils.logError("Unable to sign off. Could be already signed off.");
                timecardEditFailure = 1;
            }
        }
        else
            Utils.logError("Skipping SignOff() due to Abort signal");
    }

    @Test(dataProvider = "RemoveSignOff")
    public void removeSignOff(String removeSignoffIcon) throws Exception {		   
            wait = new WebDriverWait(driver, 30);

            // Wait for sign off group edit to complete before removing sign off
            Utils.thinkTime(10000);

            // Click on sign off icon
            driver.findElement(By.xpath(removeSignoffIcon)).click();		    		    
  }
	  
    @Test(dataProvider = "CurrentPayPeriod")
    public void currentPayPeriod(String timeframeIcon, String currentPayPeriod, String confirmTimecardload) {
        Utils.thinkTime(1000);
        wait = new WebDriverWait(driver, 30);

        try {
            activeElement = driver.findElement(By.xpath(timeframeIcon));
            activeElement.click();

            Utils.thinkTime(1000);

            // Select current pay period
            driver.findElement(By.xpath(currentPayPeriod)).click();;		    		   
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(confirmTimecardload))));
        }
        catch (Exception ex) {
            Utils.logError("Unable to confirm timecard load for current pay period. Aborting test...");
            QuickTestMain.abortTestSuite = 1;
        }
    }
    
    @Test(dataProvider = "PreviousPayPeriod")
    public void previousPayPeriod(String timeframeIcon, String previousPayPeriod, String confirmTimecardload) {
        Utils.thinkTime(1000);
        wait = new WebDriverWait(driver, 30);

        try {
            activeElement = driver.findElement(By.xpath(timeframeIcon));
            activeElement.click();

            Utils.thinkTime(1000);

            // Select current pay period
            driver.findElement(By.xpath(previousPayPeriod)).click();;		    		   
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(confirmTimecardload))));
        }
        catch (Exception ex) {
            Utils.logError("Unable to confirm timecard load for previous pay period.");
            QuickTestMain.abortTestSuite = 1;
        }
    }
    
    public void saveTimecardEdit(String action) {
        Utils.thinkTime(1000);
        wait = new WebDriverWait(driver, 10);
        try {
            wait.until(ExpectedConditions.visibilityOf(driver.findElement( By.id("saveAction"))));
            activeElement = driver.findElement( By.id("saveAction"));
            if (activeElement.isEnabled())
               driver.findElement(By.id("saveAction")).click();
        }
        catch (NoSuchElementException ex) {
            Utils.logError("Unable to save timecard edit. " );	
            QuickTestMain.abortTestSuite = 1;
        }
        if (QuickTestMain.abortTestSuite == 0)
           screenshotTimecardEdit(action);
  }

    public void screenshotTimecardEdit(String action) {
          Utils.thinkTime(1000);
          try {
                Utils.takeSnapShot(driver, "Screenshots/" + action + "_timecardEditSaved.png") ;
        } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
        }  
  }
	    
	  
    @Test(dataProvider = "TimecardTotals")
    public void timecardTotals(String timecardTotalsButton) {
        if (QuickTestMain.abortTestSuite == 0)
        {
            wait = new WebDriverWait(driver, 15);
            try {
               driver.findElement(By.xpath(timecardTotalsButton)).click();
               Utils.thinkTime(2000);
               screenshotTimecardEdit("Totals Tab");
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable to find Totals Tab on the timecard");
            }
        } else
            Utils.logError("Skipping timecardTotals() Tab due to Abort signal...");
    }
	  
    @Test(dataProvider = "TimecardAccruals")
    public void timecardAccruals(String timecardAccrualsButton) {
	wait = new WebDriverWait(driver, 10);
        if (QuickTestMain.abortTestSuite == 0)
        {
            wait = new WebDriverWait(driver, 15);
            try {
               driver.findElement(By.xpath(timecardAccrualsButton)).click();
               Utils.thinkTime(2000);
               screenshotTimecardEdit("Accruals Tab");
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable to find Totals Tab on the timecard");
            }
        } else
            Utils.logError("Skipping timecardAccruals() Tab due to Abort signal...");		    
    }
	  
    @Test(dataProvider = "TimecardAudit")
    public void timecardAudit(String timecardAuditButton) {
	wait = new WebDriverWait(driver, 10);
	if (QuickTestMain.abortTestSuite == 0)
        {
            wait = new WebDriverWait(driver, 15);
            try {
               driver.findElement(By.xpath(timecardAuditButton)).click();
               Utils.thinkTime(2000);
               screenshotTimecardEdit("Audit Tab");
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable to find Totals Tab on the timecard");
            }
        } else
            Utils.logError("Skipping timecardAudit() Tab due to Abort signal...");
    }
	  
    @Test(dataProvider = "TimecardHCorrections")
    public void timecardHCorrections(String timecardHCorrectionsButton) {
        wait = new WebDriverWait(driver, 10);
        if (QuickTestMain.abortTestSuite == 0)
        {
            wait = new WebDriverWait(driver, 15);
            try {
               driver.findElement(By.xpath(timecardHCorrectionsButton)).click();
               Utils.thinkTime(2000);
               screenshotTimecardEdit("Historical Corrections Tab");
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable to find Historical Corrections Tab on the timecard");
            }
        } else
            Utils.logError("Skipping timecardHCorrections() Tab due to Abort signal...");
    }
	  
    @Test(dataProvider = "TimecardWorkSummary")
    public void timecardWorkSummary(String timecardWorkSummaryButton) {
        wait = new WebDriverWait(driver, 10);
	if (QuickTestMain.abortTestSuite == 0)
        {
            wait = new WebDriverWait(driver, 15);
            try {
               driver.findElement(By.xpath(timecardWorkSummaryButton)).click();
               Utils.thinkTime(2000);
               screenshotTimecardEdit("Work Summary Tab");
            } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Utils.logError("Unable to find Work Summary Tab on the timecard");
            }
        } else
            Utils.logError("Skipping timecardWorkSummary() Tab due to Abort signal...");	   
    }
	  
	  //*********************************************************************************
	  // All data providers are below
	  //*********************************************************************************
	  
	  @DataProvider(name = "OpenTimecard")
	  public static Object[] openTimecardProperties() {
	      return new Object[][] {{elementProperties.getProperty("confirmtimecardload"), elementProperties.getProperty("selectempfortimecard")}};
	  }
	  
	  @DataProvider(name = "EditTimecard")
	  public static Object[] editTimecardProperties() {
	      return new Object[][] {{elementProperties.getProperty("inpunchcell"), elementProperties.getProperty("outpunchcell"), elementProperties.getProperty("savetimecard"),
	    	  elementProperties.getProperty("inpunchdtm"),  elementProperties.getProperty("outpunchdtm"),
                  elementProperties.getProperty("rowdelete")}};
	  }
	  
	  @DataProvider(name = "PunchTransferText")
	  public static Object[] punchTransferText() {
	      return new Object[][] {{elementProperties.getProperty("punchtransfercell"), elementProperties.getProperty("punchtransfertxt")}};
	  }
	  
	  @DataProvider(name = "PaycodeEditText")
	  public static Object[] paycodeEditText() {
	      return new Object[][] {{elementProperties.getProperty("paycodecell"), elementProperties.getProperty("amountcell"),
	    	  elementProperties.getProperty("paycode"), elementProperties.getProperty("amount")}};
	  }
	  
	  @DataProvider(name = "TimecardTotals")
	  public static Object[] timecardTotalsButton() {
	      return new Object[][] {{elementProperties.getProperty("timecardtotalsbutton")}};
	  }
	  
	  @DataProvider(name = "TimecardAccruals")
	  public static Object[] timecardAccrualsButton() {
	      return new Object[][] {{elementProperties.getProperty("timecardaccrualsbutton")}};
	  }
	  
	  @DataProvider(name = "TimecardAudit")
	  public static Object[] timecardAuditButton() {
	      return new Object[][] {{elementProperties.getProperty("timecardauditbutton")}};
	  }
	  
	  @DataProvider(name = "TimecardHCorrections")
	  public static Object[] timecardHCorrectionsButton() {
	      return new Object[][] {{elementProperties.getProperty("timecardhcorrectionsbutton")}};
	  }
	  
	  @DataProvider(name = "TimecardWorkSummary")
	  public static Object[] timecardWorkSummaryButton() {
	      return new Object[][] {{elementProperties.getProperty("timecardworksummarybutton")}};
	  }	   
	  
	  @DataProvider(name = "CommentsText")
	  public static Object[] punchComment() {
	      return new Object[][] {{elementProperties.getProperty("timecardcommentsicon"), elementProperties.getProperty("timecardcommentstextbox"),
	    	  elementProperties.getProperty("comment"), elementProperties.getProperty("commentapply"), 
	    	  elementProperties.getProperty("outpunchcell"), elementProperties.getProperty("inpunchcell")}};
	  }
	  
	  @DataProvider(name = "ReviewExceptions")
	  public static Object[] reviewExceptions() {
	      return new Object[][] {{elementProperties.getProperty("markasreviewedicon"), 
                  elementProperties.getProperty("exception1"), elementProperties.getProperty("exception2"), 
                  elementProperties.getProperty("exception3"), elementProperties.getProperty("exception4")}};
	  }
	  
	  @DataProvider(name = "SignOff")
	  public static Object[] signOff() {
	      return new Object[][] {{elementProperties.getProperty("signofficon")}};
	  }
	  
	  @DataProvider(name = "RemoveSignOff")
	  public static Object[] removeSignOff() {
	      return new Object[][] {{elementProperties.getProperty("removesignofficon")}};
	  }
	  
	  @DataProvider(name = "CurrentPayPeriod")
	  public static Object[] currentPayPeriod() {
	      return new Object[][] {{elementProperties.getProperty("timeframeicon"), elementProperties.getProperty("currentpayperiod"),
                                     elementProperties.getProperty("confirmtimecardload")}};
	  }
	  
	  @DataProvider(name = "PreviousPayPeriod")
	  public static Object[] previousPayPeriod() {
	      return new Object[][] {{elementProperties.getProperty("timeframeicon"), elementProperties.getProperty("previouspayperiod"),
                                      elementProperties.getProperty("confirmtimecardload")}};
	  }
	  
	  @DataProvider(name = "NextPayPeriod")
	  public static Object[] nextPayPeriod() {
	      return new Object[][] {{elementProperties.getProperty("timeframeicon"), elementProperties.getProperty("nextpayperiod"),
                                      elementProperties.getProperty("confirmtimecardload")}};
	  }
}